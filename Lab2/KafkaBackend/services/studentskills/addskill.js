const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let addSkill = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("In posting skills");
  var studentId;
  Decryptedtoken = decryptToken(msg.auth);
  try {
    console.log(Decryptedtoken.email);

    await Student.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        if (tokenuser) {
          tokenuser.skills.push({ skill_name:msg.student.skill_name });

          tokenuser.save(err => {
            if (err) {
              res.status(403).send({
                errors: {
                  err: "Unable to add school"
                }
              });
            } else {
           
            let data = { skills: tokenuser.skills };
            (response.data = data.skills), (response.status = 201);
            return callback(null, response);
            }
          });
        } else {
            err.status = 403;
            err.data = {
              errors: {
                body: "Unauthenticated user"
              }
            };
            return callback(err, null);
        }
      })
      .catch(err => {
        console.log(`error getting student basic details ${err}`);
      });
  } catch (err) {
    console.log(err);
    err.status = 403;
      err.data = {
        errors: {
          body: "Unauthenticated user"
        }
      };
      return callback(err, null);
  }
};

exports.addSkill = addSkill;

const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let deleteSkill = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log();
  console.log("In deleting skill");
  var studentId;
  Decryptedtoken = decryptToken(msg.auth);
  try {
    await Student.findOne({
      emailId: Decryptedtoken.email
    })

      .then(async tokenuser => {
        var skillarr = tokenuser.skills.filter(
          e => e._id != msg.data.skills.skill_id
        );
        console.log(skillarr);
        const filter = { emailId: Decryptedtoken.email };
        const update = { skills: skillarr };
        await Student.findOneAndUpdate(filter, update, {
          new: true,
          useFindAndModify: true
        }).then(res1 => {
          let data = { skills: res1.skills };
            (response.data = data.skills), (response.status = 201);
            return callback(null, response);
        });
      })
      .catch(err => {
        console.log(`error getting student basic details ${err}`);
      });
  } catch (error) {
    console.log(error + "error sdsad");
    err.status = 403;
            err.data = {
              errors: {
                body: "Unauthenticated user"
              }
            };
            return callback(err, null);
  }
};

exports.deleteSkill = deleteSkill;

"use strict";

const { addSkill } = require("./addskill");
const {deleteSkill} = require("./deleteskill")

let handle_request = (msg, callback) => {
  switch (msg.route) {
    case "post_studentskill":
      addSkill(msg, callback);
      break;
    case "delete_studentskill":
        deleteSkill(msg,callback);
        break;
  }
};

exports.handle_request = handle_request;

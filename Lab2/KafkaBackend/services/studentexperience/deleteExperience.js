const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let deleteExperience = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("In deleting experience");
  var studentId;
  Decryptedtoken = decryptToken(msg.auth);
  try {
    await Student.findOne({
      emailId: Decryptedtoken.email
    })
      .then(async tokenuser => {
        var filterexperience = tokenuser.experiences.filter(
          e => e._id != msg.data.experience.job_id
        );
        console.log(filterexperience);
        const filter = { emailId: Decryptedtoken.email };
        const updatearr = { experiences: filterexperience };
        await Student.findOneAndUpdate(filter, updatearr, {
          new: true,
          useFindAndModify: true
        }).then(res1 => {
            (response.data = res1.experiences), (response.status = 201);
            return callback(null, response);
        });
      })
      .catch(err => {
        console.log(`error getting student basic details ${err}`);
      });
  } catch (err) {
    console.log(err + "error sdsad");
   

    err.status = 500;
    err.data = {
      errors: {
        body:  "cannot delete as record is not present"
      }
    };
    return callback(err, null);
  }
};

exports.deleteExperience = deleteExperience;

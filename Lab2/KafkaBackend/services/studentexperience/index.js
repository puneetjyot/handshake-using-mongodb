"use strict";
const { postExperience } = require("./postExperience");
const { editExperience } = require("./editExperience");
const { deleteExperience } = require("./deleteExperience");
let handle_request = (msg, callback) => {
  switch (msg.route) {
    case "post_experience":
      postExperience(msg, callback);
      break;
    case "edit_experience":
      editExperience(msg, callback);
      break;
      case "delete_experience":
      deleteExperience(msg, callback);
      break;
  }
};

exports.handle_request = handle_request;

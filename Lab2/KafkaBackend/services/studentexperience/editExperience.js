const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let editExperience = async (msg, callback) => {
  let response = {};
  let err = {};
  Decryptedtoken = decryptToken(msg.auth);
  if (Decryptedtoken.email !== null) {
    await Student
      .findOne({
      
          emailId: Decryptedtoken.email
        
      })
      .then(async tokenuser => {
        var experiencearr=tokenuser.experiences;
        var updatedExperience=experiencearr.filter(e=>e._id==msg.experience.job_id)
        console.log(updatedExperience)
        var restExperience=experiencearr.filter(e=>e._id!=msg.experience.job_id)
        console.log(restExperience)
        var update={
         
              
                job_title: (msg.experience.job_title)?msg.experience.job_title:updatedExperience.job_id,
                employer: (msg.experience.employer)?msg.experience.employer:updatedExperience.employer,
                start_time: (msg.experience.start_time)?msg.experience.start_time:updatedExperience.start_time,
                end_time: (msg.experience.end_time)?msg.experience.end_time:updatedExperience.end_time,
                location: (msg.experience.location)?msg.experience.location:updatedExperience.location,
                description: (msg.experience.description)?msg.experience.description:updatedExperience.description
        }

      

      restExperience.push(update);
      console.log(restExperience)
      const filter={ emailId: Decryptedtoken.email}
      const updatearr={experiences: restExperience}
      await Student.findOneAndUpdate(filter,updatearr,{new:true,useFindAndModify:true})
      .then(res1=>{
       (response.data = res1.experiences), (response.status = 201);
              return callback(null, response);
      })

      .catch(err => {
        console.log(`error getting student basic details ${err}`);
      });
    })
  } else {
    err.status = 403;
    err.data = {
      errors: {
        message: [Decryptedtoken.error]
      }
    };
    return callback(err, null);
  }

};

exports.editExperience = editExperience;

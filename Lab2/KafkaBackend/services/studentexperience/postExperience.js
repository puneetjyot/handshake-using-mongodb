const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let postExperience = async (msg, callback) => {
  let response = {};
  let err = {};
  Decryptedtoken = decryptToken(msg.auth);
  try {
    if (Decryptedtoken.email !== null) {
      await Student.findOne({
        emailId: Decryptedtoken.email
      })
        .then(tokenuser => {
          console.log(tokenuser);

          tokenuser.experiences.push({
            job_id: msg.experience.job_id,
            job_title: msg.experience.job_title,
            employer: msg.experience.employer,
            start_time: msg.experience.start_time,
            end_time: msg.experience.end_time,
            location: msg.experience.location,
            description: msg.experience.description
          });
          tokenuser.save(err => {
            if (err) {
              err.status = 500;
              err.data = {
                errors: {
                  body: "Server error"
                }
              };
              return callback(err, null);
            } else {
              (response.data = tokenuser.experiences), (response.status = 201);
              return callback(null, response);
            }
          });
        })
        .catch(err => {
          console.log(`error getting student basic details ${err}`);
        });
    } else {
      err.status = 403;
      err.data = {
        errors: {
          message: [Decryptedtoken.error]
        }
      };
      return callback(err, null);
    }
  } catch (err) {
    console.log(err);
    err.status = 500;
    err.data = {
      errors: {
        body: "Server error"
      }
    };
    return callback(err, null);
  }
};

exports.postExperience = postExperience;

const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let postBasicDetails = async (msg, callback) => {
  let response = {};
  let err = {};
  Decryptedtoken = decryptToken(msg.auth);
  console.log(msg.basicdetails.studentstate);
  try {
    console.log(Decryptedtoken.email);
    await Student.findOne({
      emailId: Decryptedtoken.email
    })
      .then(async tokenuser => {
        const filter = { emailId: Decryptedtoken.email };

        const result = await Student.findOneAndUpdate(
          filter,

          {
            dob: msg.basicdetails.dob ? msg.basicdetails.dob : tokenuser.dob,
            state: msg.basicdetails.studentstate
              ? msg.basicdetails.studentstate
              : tokenuser.studentstate,
            city: msg.basicdetails.city
              ? msg.basicdetails.city
              : tokenuser.city,
            country: msg.basicdetails.country
              ? msg.basicdetails.country
              : tokenuser.country,
            email: msg.basicdetails.email
              ? msg.basicdetails.email
              : tokenuser.emailId,
            phone: msg.basicdetails.phone
              ? msg.basicdetails.phone
              : tokenuser.phone
          },
          { new: true, useFindAndModify: true }
        );
        let basicdata = {
          result: {
            student: {
              email: msg.basicdetails.email
                ? msg.basicdetails.email
                : tokenuser.emailId,
              student_basic_details: {
                dob: msg.basicdetails.dob
                  ? msg.basicdetails.dob
                  : tokenuser.dob,
                city: msg.basicdetails.city
                  ? msg.basicdetails.city
                  : tokenuser.city,
                state: msg.basicdetails.studentstate
                  ? msg.basicdetails.studentstate
                  : tokenuser.studentstate,
                country: msg.basicdetails.country
                  ? msg.basicdetails.country
                  : tokenuser.country,
                phone: msg.basicdetails.phone
                  ? msg.basicdetails.phone
                  : tokenuser.phone_number
              }
            }
          }
        };
     
        (response.data = basicdata), (response.status = 201);
        return callback(null, response);
      })
      .catch(err => {
        console.log(`error posting student journey ${err}`);
      });
  } catch (err) {
    console.log(`error posting student journey ${err}`);
    err.status = 500;
    err.data = {
      errors: {
        body: "Server error"
      }
    };
    return callback(err, null);
  }
};

exports.postBasicDetails = postBasicDetails;

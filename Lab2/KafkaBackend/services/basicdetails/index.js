"use strict";
const { postBasicDetails } = require("./postBasicDetails");

let handle_request = (msg, callback) => {
  switch (msg.route) {
    case "post_basic_details":
      postBasicDetails(msg, callback);
      break;
   
  }
};

exports.handle_request = handle_request;

const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let editEducation = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("In updating name");
  var studentId;
  Decryptedtoken = decryptToken(msg.auth);
  try {
    console.log(msg.education.gpa);
    const preeducation = await Student.findOne({
      emailId: Decryptedtoken.email
    });
    console.log(preeducation.educations);
    // var doc=preeducation.educations._id(msg.education.educationId)
    var educationarr = preeducation.educations;
    filteredEducation = educationarr.filter(
      e => e._id == msg.education.educationId
    );
    restEducation = educationarr.filter(
      e => e._id != msg.education.educationId
    );
    console.log("filtered=> " + filteredEducation);
    console.log(restEducation);

    const update = {
      school_name: msg.education.schoolname
        ? msg.education.schoolname
        : filteredEducation.school_name,
      education_level: msg.education.educationlevel
        ? msg.education.educationlevel
        : filteredEducation.education_level,
      major: msg.education.major
        ? msg.education.major
        : filteredEducation.major,
      minor: msg.education.minor ? msg.education.minor : "",
      start_time: msg.education.startDate
        ? msg.education.startDate
        : filteredEducation.start_time,
      end_time: msg.education.endDate
        ? msg.education.endDate
        : filteredEducation.end_time,
      gpa: msg.education.gpa ? msg.education.gpa : filteredEducation.GPA,
      isPrimary: msg.education.isPrimary
        ? msg.education.isPrimary
        : filteredEducation.isPrimary
    };

    restEducation.push(update);
    console.log(restEducation);

    const filter = { emailId: Decryptedtoken.email };
    const updatearr = { educations: restEducation };
    await Student.findOneAndUpdate(filter, updatearr, {
      new: true,
      useFindAndModify: true
    }).then(res1 => {
      let data = {
        educations: res1.educations
      };
      (response.data = data.educations), (response.status = 201);
      return callback(null, response);
    });
  } catch (err) {
    console.log(err);
    err.status = 403;
    err.data = {
      errors: {
        body: "Unauthenticated user"
      }
    };
    return callback(err, null);
  }
};

exports.editEducation = editEducation;

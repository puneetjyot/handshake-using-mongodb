
const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let postJourney = async (msg, callback) => {
  let response = {};
  let err = {};
    console.log("getting journey")
  Decryptedtoken = decryptToken(msg.auth);
  try {
    const filter = { emailId: Decryptedtoken.email };
    const update = { career_objective:msg.student.career_objective };
    await Student.findOneAndUpdate(filter, update, {
      new: true,
      useFindAndModify: true
    })
      .then(tokenuser => {
          console.log("in error")
        if (tokenuser) {
            
            var data={result: tokenuser.career_objective};
            (response.data = data), (response.status = 201);
            return callback(null, response);
        } else {
            err.status = 401;
            err.data = {
              errors: {
                body: "Unauthenticated User"
              }
            };
            return callback(err, null);
            
        
        }
      })

      .catch(err => {
        console.log(`error posting student journey ${err}`);
      });
  } catch (err) {
    console.log(`error posting student journey ${err}`);
    err.status = 401;
            err.data = {
              errors: {
                body: err
              }
            };
            return callback(err, null);
  }
 
  
};

exports.postJourney = postJourney

const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let editName = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("In updating name");
  Decryptedtoken = decryptToken(msg.auth);
  try {
    const filter = { emailId: Decryptedtoken.email };
    const update = { name: msg.student.name };

    await Student.findOneAndUpdate(filter, update, {
      new: true,
      useFindAndModify: true
    })
      .then(tokenuser => {
       let data={tokenuser}
          response.data=data,response.status=201
          return callback(null, response);
        
      })
      .catch(err => {
        console.log(`error getting student basic details ${err}`);
      });
  } catch (error) {
    err.status = 401;
    err.data = {
      errors: {
        body: error
      }
    };
    return callback(err, null);

  }
 
  
};

exports.editName = editName
"use strict";

const { postJourney } = require("./postjourney");
const { getJourney } = require("./getjourney");
const { editName } = require("./editName");
const { addEducation } = require("./addEducation");
const { editEducation } = require("./editEducation");
const { deleteEducation } = require("./deleteEducation");
const { updateProfilePic } = require("./updateProfilePic");
let handle_request = (msg, callback) => {
  console.log("in here");
  switch (msg.route) {
    case "post_studentjourney":
      postJourney(msg, callback);
      break;
    case "get_studentjourney":
      getJourney(msg, callback);
      break;
    case "edit_name":
      editName(msg, callback);
      break;
    case "add_education":
      addEducation(msg, callback);
      break;
    case "edit_education":
      editEducation(msg, callback);
      break;
      case "delete_education":
      deleteEducation(msg, callback);
      break;
      case "update_profilepic":
        updateProfilePic(msg, callback);
      break;
  }
};

exports.handle_request = handle_request;

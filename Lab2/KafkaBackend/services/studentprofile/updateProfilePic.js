const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let updateProfilePic = async (msg, callback) => {
    let response = {};
    let err = {}; 
    var studentId;
  Decryptedtoken = decryptToken(msg.auth);
  try {
    await Student.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        console.log(tokenuser.student_basic_detail_id + "in details");
        studentId = tokenuser.student_basic_detail_id;
        email = tokenuser.emailId;
        name = tokenuser.name;
      })
      .catch(err => {
        console.log(`error getting student basic details ${err}`);
      });

    const result = await Student.findOneAndUpdate(
      { student_basic_detail_id: studentId },
      { profile_picture: msg.originalname },
      { new: true, useFindAndModify: true }
    );

    if (result) {
      console.log(result);
      let data = { name: msg.originalname };
      // res.status(201).send({ name: msg.originalname });
      (response.data = data), (response.status = 201);
      return callback(null, response);
    } else {
      err.status = 403;
      err.data = {
        errors: {
          body: "Unauthenticated user"
        }
      };
      return callback(err, null);
    }
  } catch (err) {
    console.log(err);
    err.status = 403;
    err.data = {
      errors: {
        body: "Unauthenticated user"
      }
    };
    return callback(err, null);
  }
};

exports.updateProfilePic = updateProfilePic;

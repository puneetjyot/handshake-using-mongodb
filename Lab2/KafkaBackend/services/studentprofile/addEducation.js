const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let addEducation = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("In updating name");
  var studentId;
  Decryptedtoken = decryptToken(msg.auth);
  try {
    await Student.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        if (tokenuser) {
          tokenuser.educations.push({
            school_name: msg.education.schoolname,
            education_level: msg.education.educationlevel,
            major: msg.education.major,
            minor: msg.education.minor ? msg.education.minor : "",
            start_time: msg.education.startDate,
            end_time: msg.education.endDate,
            gpa: msg.education.gpa,
            isPrimary: msg.education.isPrimary
          });
          tokenuser.save(err => {
            if (err) {
              err.status = 500;
              err.data = {
                errors: {
                  body: "Unable to add school"
                }
              };
              return callback(err, null);
            } else {
              let data = {
                educations: tokenuser.educations
              };
              (response.data = data.educations), (response.status = 201);
              return callback(null, response);
            }
          });
        } else {
          err.status = 403;
          err.data = {
            errors: {
              body: "Unauthenticated user"
            }
          };
          return callback(err, null);
        }
      })
      .catch(err => {
        console.log(`error getting student basic details ${err}`);
      });
  } catch (err) {
    console.log(err);
    err.status = 403;
    err.data = {
      errors: {
        body: "Unauthenticated user"
      }
    };
    return callback(err, null);
  }
};

exports.addEducation = addEducation;

const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let deleteEducation = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("In deleting name");
  var studentId;
  Decryptedtoken = decryptToken(msg.auth);
  try {
    await Student.findOne({
      emailId: Decryptedtoken.email
    })
      .then(async tokenuser => {
        studentId = tokenuser.student_basic_detail_id;
        var filtereducation = tokenuser.educations.filter(
          e => e._id != msg.data.education.educationId
        );
        console.log(filtereducation);
        const filter = { emailId: Decryptedtoken.email };
        const updatearr = { educations: filtereducation };
        await Student.findOneAndUpdate(filter, updatearr, {
          new: true,
          useFindAndModify: true
        }).then(res1 => {
            (response.data = res1.educations), (response.status = 201);
            return callback(null, response);
        });
      })
      .catch(err => {
        console.log(`error getting student basic details ${err}`);
      });
  } catch (error) {
    console.log(error + "error sdsad");
    err.status = 403;
    err.data = {
      errors: {
        body: "Unauthenticated user"
      }
    };
    return callback(err, null);
  }
};

exports.deleteEducation = deleteEducation;

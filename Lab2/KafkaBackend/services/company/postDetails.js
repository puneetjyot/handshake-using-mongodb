const { Student } = require("../../db/studentmodel");
const { Company } = require("../../db/comapnymodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let postDeatils = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("----------posting company details in real");
  Decryptedtoken = decryptToken(msg.auth);
  console.log(Decryptedtoken.email);
  try {
    await Company.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        console.log(tokenuser);

        companyId = tokenuser.company_basic_detail_id;
        email = tokenuser.emailId;
        name = tokenuser.company_name;
      })
      .catch(err => {
        console.log(`error posting company details ${err}`);
      });
    const filter = { company_basic_detail_id: companyId };
    const update = { description: msg.company.description };
    const result = await Company.findOneAndUpdate(filter, update, {
      new: true
    });

    if (result) {
      console.log(result);
      (response.data = result), (response.status = 201);
      return callback(null, response);
    } else {
        err.status = 500;
        err.data = {
          errors: {
            body: "Server error"
          }
        };
    }
  } catch (err) {
    err.status = 500;
    err.data = {
      errors: {
        body: "Server error"
      }
    };
  }
};

exports.postDeatils = postDeatils;

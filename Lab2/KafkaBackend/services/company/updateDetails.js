const { Student } = require("../../db/studentmodel");
const { Company } = require("../../db/comapnymodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let updateDetails = async (msg, callback) => {
  let response = {};
  let err = {};
  
  console.log("In updating company");
  var studentId;
  Decryptedtoken = decryptToken(msg.auth);
  console.log(Decryptedtoken.email);
  try {
    let name, companyId, phone;
    await Company.findOne({
      emailId: Decryptedtoken.email
    }).then(async tokenuser => {
      companyId = tokenuser.company_basic_detail_id;
      name = msg.company.name
        ? msg.company.name
        : tokenuser.company_name;
      location = msg.company.location
        ? msg.company.location
        : tokenuser.location;
      phone = msg.company.phone ? msg.company.phone : tokenuser.phone;

      const filter = { company_basic_detail_id: companyId };

      const update = {
        company_name: name,
        location: location,
        phone: phone
      };
      const result = await Company.findOneAndUpdate(filter, update, {
        new: true
      });

      if (result) {
        console.log(result);
    let companydata={
          company: {
          company_basic_details: result
        }
      };
      (response.data = companydata), (response.status = 201);
      return callback(null, response);
      } else {
        err.status = 403;
      err.data = {
        errors: {
          message: "unable to add jobs"
        }
      };
      return callback(err, null);
      }
    });
  } catch (err) {
    console.log(err);
    err.status = 403;
      err.data = {
        errors: {
          message: "unable to add jobs"
        }
      };
      return callback(err, null);
  }
};

exports.updateDetails = updateDetails;

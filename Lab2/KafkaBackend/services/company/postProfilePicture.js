const { Student } = require("../../db/studentmodel");
const { Company } = require("../../db/comapnymodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let postProfilePicture = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log(JSON.stringify(msg) + " file post");

  var companyId;
  Decryptedtoken = decryptToken(msg.auth);
  try {
    await Company.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        if (tokenuser) {
          console.log(tokenuser.company_basic_detail_id + "in details");
          companyId = tokenuser.company_basic_detail_id;
          email = tokenuser.emailId;
          name = tokenuser.name;
        } else {
          
          err.status = 403;
          err.data = {
            errors: {
                err: "unauthenticated user"
              }
          };
          return callback(err, null);
        }
      })
      .catch(err => {
        console.log(`error getting company basic details ${err}`);
      });

    const filter = { company_basic_detail_id: companyId };
    const update = { profilepicaddress: msg.originalname };

    const result = await Company.findOneAndUpdate(filter, update, {
      new: true,
      useFindAndModify: true
    });

    if (result) {
     
     let companydata={
         name: msg.originalname 
      };
      (response.data = companydata), (response.status = 201);
      return callback(null, response);
    } else {
     
        err.status = 403;
          err.data = {
            errors: {
                err: "Unable to add company photo"
              }
          };
          return callback(err, null);
       
      
    }
  } catch (error) {
    console.log(error);
    err.status = 403;
    err.data = {
      errors: {
          err: "Unable to add company photo"
        }
    };
    return callback(err, null);
  }
};

exports.postProfilePicture = postProfilePicture;

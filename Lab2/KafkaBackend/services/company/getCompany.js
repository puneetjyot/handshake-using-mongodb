const { Student } = require("../../db/studentmodel");
const { Company } = require("../../db/comapnymodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let getCompany = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("----------getting company details");
  Decryptedtoken = decryptToken(msg.auth);
  console.log(Decryptedtoken.email);
  try {
    await Company.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        if (tokenuser) {
           let result={
                company: {
                    company_basic_details: tokenuser
                  }
            };
            (response.data = result), (response.status = 201);
      return callback(null, response);
         
        } else {
            err.status = 500;
            err.data = {
              errors: {
                body: "Server error"
              }
            };
        }
      })
      .catch(err => {
        console.log(`error posting student journey ${err}`);
      });
    //console.log(companyId);
  } catch (err) {
    console.log(err);
    err.status = 500;
    err.data = {
      errors: {
        body: "Server error"
      }
    };
  }
};

exports.getCompany = getCompany;

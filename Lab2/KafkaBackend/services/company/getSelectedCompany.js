const { Student } = require("../../db/studentmodel");
const { Company } = require("../../db/comapnymodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let getSelectedCompany = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("----------getting particular company details");
  console.log(msg.auth+" this is the header")
  Decryptedtoken = decryptToken(msg.auth);
  try {
    var studentId;
    await Student.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        if (tokenuser) {
          student = tokenuser;
          studentId = tokenuser.company_basic_detail_id;
          email = tokenuser.emailId;
        } else {
          err.status = 403;
          err.data = {
            errors: {
              message: "unauthenticated user"
            }
          };
          return callback(err, null);
        }
      })
      .catch(err => {
        console.log(`error getting company details ${err}`);
      });

    const companybasic = await Company.findOne({
      company_basic_detail_id: msg.id
    });

    if (companybasic) {
      let companydata = {
        company: {
          company_basic_details: companybasic
        }
      };
      (response.data = companydata), (response.status = 201);
      return callback(null, response);
    } else {
      err.status = 403;
      err.data = {
        errors: {
          err: "Unable to find company"
        }
      };
      return callback(err, null);
    }
  } catch (error) {
    console.log(error);
    err.status = 403;
    err.data = {
      errors: {
        err: "Unable to find company"
      }
    };
  }
};

exports.getSelectedCompany = getSelectedCompany;

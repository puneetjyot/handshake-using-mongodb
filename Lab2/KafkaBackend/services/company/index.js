"use strict";
const {getCompany  } = require("./getCompany");
const { getSelectedCompany } = require("./getSelectedCompany");
const { postDeatils } = require("./postDetails");
const { postProfilePicture } = require("./postProfilePicture");
const { updateDetails } = require("./updateDetails");

let handle_request = (msg, callback) => {
  switch (msg.route) {
    case "get_company":
      getCompany(msg, callback);
      break;
      case "get_selected_company":
        getSelectedCompany(msg, callback);
      break;
      case "post_details":
        postDeatils(msg, callback);
      break;
      case "post_profile_picture":
        postProfilePicture(msg, callback);
      break;
      case "update_details":
        updateDetails(msg, callback);
      break;
   
  }
};

exports.handle_request = handle_request;

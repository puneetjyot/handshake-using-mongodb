const { Student } = require("../../db/studentmodel");
const bcrypt = require("bcrypt");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let studentLogin = async (msg, callback) => {
  let response = {};
  let err = {};

  console.log(msg.student.email);
  console.log("In login");

  const studenttoken = await generateToken(msg.student.email);
  try {
    const student = await Student.findOne({
      emailId: msg.student.email
    });
    // console.log(student)
    if (student) {
      bcrypt.compare(msg.student.password, student.password, function(
        err,
        isMatch
      ) {
        console.log(bcrypt.hashSync(msg.student.password, 10));
        console.log(student.password);
        if (err) {
          err.status = 500;
          err.data = {
            errors: {
              body: err
            }
          };
          return callback(err, null);
        } else if (!isMatch) {
          err.status = 403;
          err.data = {
            errors: {
              body: "Unauthenticated User"
            }
          };
          return callback(err, null);
        } else {
          let data = {
            user: {
              emailId: student.email,
              name: student.name,
              image: null,
              token: studenttoken,
              resp: student,
              isLogin: true
            }
          };
          (response.data = data), (response.status = 201);
          return callback(null, response);
        }
      });
    } else {
      err.status = 401;
      err.data = {
        errors: {
          body: "Unauthorised User"
        }
      };
      return callback(err, null);
    }
  } catch (err) {
    err.status = 500;
    err.data = {
      errors: {
        body: err
      }
    };
    return callback(err, null);
  }
};

exports.studentLogin=studentLogin
const { Company } = require("../../db/comapnymodel");
const bcrypt = require("bcrypt");
const { generateToken, decryptToken } = require("../../service/tokenservice");
const { generateUUID } = require("../../service/uuidservice");
let companyRegister = async (msg, callback) => {
  let response = {};
  let err = {};

  const companyid = await generateUUID();
  const companytoken = await generateToken(msg.company.email);
  console.log(msg.company);

  try {
    const registerCompany = await Company.create({
      emailId: msg.company.email,
      password: bcrypt.hashSync(msg.company.password, 10),
      company_basic_detail_id: companyid,
      company_name: msg.company.company_name,
      location: msg.company.location,
      phone: msg.company.phone
    });
     console.log(registerCompany);

    let data = {
      company: {
        email: registerCompany.email,
        company_name: registerCompany.company_name,
        image: null,
        token: companytoken,
        res: registerCompany
      }
    };
    response.data = data;
    response.status = 201;
    console.log("not in error")
    return callback(null, response);
  } catch (error) {
    console.log("in error"+error)
   err.data={
      errors: {
        body: error
      }
   }
   err.status=403;
  }
  return callback(err, null);
};

exports.companyRegister = companyRegister;

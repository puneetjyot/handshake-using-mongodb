const { Student } = require("../../db/studentmodel");
const bcrypt = require("bcrypt");
const { generateToken, decryptToken } = require("../../service/tokenservice");
const { generateUUID } = require("../../service/uuidservice");
let studentRegister = async (msg, callback) => {
  let response = {};
  let error = {};

  const studentid = await generateUUID();
  const studenttoken = await generateToken(msg.student.email);
  console.log(msg.student.email);

  try {
    const registerStudent = await Student.create({
      emailId: msg.student.email,
      password: bcrypt.hashSync(msg.student.password, 10),
      student_basic_detail_id: studentid,
      name: msg.student.name,
      college: msg.student.college
    });
    console.log(msg.student.name);

    let data = {
      user: {
        email: registerStudent.email,
        name: registerStudent.name,
        image: null,
        token: studenttoken,
        isRegister: true,
        resp: registerStudent
      }
    };
    response.data = data;
    response.status = 201;
    return callback(null, response);
  } catch (err) {
    error.status = 401;
    error.data = {
      errors: {
        body: err
      }
    };
    return callback(err, null);
  }
};

exports.studentRegister = studentRegister;

"use strict";
const { studentLogin } = require("./studentLogin");
const { companyLogin } = require("./companyLogin");
const {companyRegister} =require("./companyRegister")
const {studentRegister} =require("./studentRegister")
let handle_request = (msg, callback) => {
  switch (msg.route) {
    case "login_student":
      studentLogin(msg, callback);
      break;
    case "login_company":
      companyLogin(msg, callback);
      break;
      case "register_student":
      studentRegister(msg, callback);
      break;
      case "register_company":
      companyRegister(msg, callback);
      break;
  }
};

exports.handle_request = handle_request;

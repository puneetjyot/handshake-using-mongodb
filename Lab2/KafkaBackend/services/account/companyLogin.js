const { Company } = require("../../db/comapnymodel");
const bcrypt = require("bcrypt");
const { generateToken, decryptToken } = require("../../service/tokenservice");



let companyLogin = async (msg, callback) => {
  let response = {};
  let err = {};

  console.log(msg.company.email);
  console.log("In login");

  const companytoken = await generateToken(msg.company.email);
  try {
    const company = await Company.findOne({
      emailId: msg.company.email
    });
   
    if (company) {
      bcrypt.compare(msg.company.password, company.password, function(
        err,
        isMatch
      ) {
        console.log(bcrypt.hashSync(msg.company.password, 10));
        console.log(company.password);
        if (err) {
          err.status = 500;
          err.data = {
            errors: {
              body: err
            }
          };
          return callback(err, null);
        } else if (!isMatch) {
          err.status = 403;
          err.data = {
            errors: {
              body: "Unauthenticated User"
            }
          };
          return callback(err, null);
        } else {
          let data = {
            user: {
              emailId: company.email,
              name: company.name,
              image: null,
              token: companytoken,
              resp: company,
              isLogin: true
            }
          };
          (response.data = data), (response.status = 201);
          return callback(null, response);
        }
      });
    } else {
      err.status = 401;
      err.data = {
        errors: {
          body: "Unauthorised User"
        }
      };
      return callback(err, null);
    }
  } catch (err) {
    err.status = 500;
    err.data = {
      errors: {
        body: err
      }
    };
    return callback(err, null);
  }
};

exports.companyLogin = companyLogin
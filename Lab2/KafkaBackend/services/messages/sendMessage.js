const { Student } = require("../../db/studentmodel");
const { Messages } = require("../../db/messageModel");

const { generateToken, decryptToken } = require("../../service/tokenservice");
const { Company, Event } = require("../../db/comapnymodel");
const { StudentEvents } = require("../../db/eventmodel");
let sendMessage = async (msg, callback) => {
  let response = {};
  let err = {};

  try {
    Decryptedtoken = decryptToken(msg.auth);
    let messageobj = {
      Body: msg.message.body,
      senderId: msg.params.senderId
    };
    const getOldChat = await Messages.findOne({
      $and: [
        {
          $or: [
            { Sender: msg.params.senderId },
            { Sender: msg.params.receiverId }
          ]
        },
        {
          $or: [
            { Receiver: msg.params.senderId },
            { Receiver: msg.params.receiverId }
          ]
        }
      ]
    });
    console.log(getOldChat)
    if (getOldChat) {
      getOldChat.MessageArray.push(messageobj);
      getOldChat.save();
      // const filter = { _id: getOldChat._id };
      // const update = { MessageArray: messageArray };
      // const updateMessageArray = Messages.findOneAndUpdate(filter, update, {
      //   new: true,
      //   useFindAndModify: true
      // });
      (response.data = getOldChat), (response.status = 201);
      return callback(null, response);
    }
    else{
      // let temparr=[];
      // temparr.push(messageobj)
    const message = await Messages.create({
    Sender:msg.params.senderId,
    Receiver: msg.params.receiverId,
    SenderModel:msg.params.senderModel,
    ReceiverModel:msg.params.receiverModel,
    // MessageArray:temparr
    });


    if(message){
      message.MessageArray.push(messageobj)
      message.save();
    (response.data = message), (response.status = 201);
    return callback(null, response);
    }
  }
  } catch (error) {
    console.log(error);
    err.status = 500;
    err.data = {
      errors: {
        body: err
      }
    };
    return callback(err, null);
  }
};

exports.sendMessage = sendMessage;

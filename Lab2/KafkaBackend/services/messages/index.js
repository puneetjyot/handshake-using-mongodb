"use strict";
const{getMessage} =require("./getMessage")
const{sendMessage} =require("./sendMessage")
const{getMessageByStudent} =require("./getMessageByStudent")

let handle_request = (msg, callback) => {
    console.log(msg.route)
  switch (msg.route) {
    case "get_message_by_student":
      getMessageByStudent(msg, callback);
      break;
    case "get_messages":
      getMessage(msg, callback);
      break;
    case "send_message":
      sendMessage(msg, callback);
      break;
  }
};

exports.handle_request = handle_request;

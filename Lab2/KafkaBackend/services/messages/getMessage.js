const { Student } = require("../../db/studentmodel");
const {Messages} = require("../../db/messageModel")
const { generateToken, decryptToken } = require("../../service/tokenservice");
const { Company, Event } = require("../../db/comapnymodel");
const { StudentEvents } = require("../../db/eventmodel");
let getMessage = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("Getting messages")
  
  try {
    Decryptedtoken = decryptToken(msg.auth);
   

   const studentArr1= await Messages.find({
        Sender:msg.id
    }).populate("Receiver")

    const studentArr2=   await Messages.find({
        Receiver:msg.id
    }).populate("Sender")

    studentArr2.map(i=>{
        var tempObj=i.Sender;
        i.Sender=i.Receiver
        i.Receiver=tempObj
    })

    var studentArr= studentArr1.concat(studentArr2);
    (response.data = studentArr), (response.status = 201);
    return callback(null, response);


  } catch (error) {
      console.log(error)
    err.status = 500;
    err.data = {
      errors: {
        body: err
      }
    };
    return callback(err, null);
  }
};

exports.getMessage = getMessage;

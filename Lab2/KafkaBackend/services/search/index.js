"use strict";
const { getStudent } = require("./getStudent");
const { getVisitedStudent } = require("./getVisitedStudent");
const { getAllStudents } = require("./getAllStudents");
let handle_request = (msg, callback) => {
  switch (msg.route) {
    case "get_student":
      getStudent(msg, callback);
      break;
    case "get_visitedstudent":
      getVisitedStudent(msg, callback);
      break;
    case "get_all_students":
      getAllStudents(msg, callback);
      break;
  }
};

exports.handle_request = handle_request;


const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
let getVisitedStudent = async (msg, callback) => {
  let response = {};
  let err = {};

 
  Decryptedtoken = decryptToken(msg.auth);

  console.log(Decryptedtoken.email);
  if (Decryptedtoken.email !== null) {
    const student = await Student.findOne({
      student_basic_detail_id:msg.id
    });
    if(student){
   data= {
     _id:student._id,
      email: student.emailId,
      name: student.name,
      career_objective: student.career_objective,
      profile_picture: student.profile_picture,
      education: student.educations,
      skills: student.skills,
      experience: student.experiences,
      token: msg.auth,
      college: student.college,
      city: student.city,
      country: student.country,
      phone: student.phone,
      dob: student.dob,
      state: student.state
    };
    (response.data = data), (response.status = 201);
    return callback(null, response);
}
else{
    err.status = 401;
    err.data = {
      errors: {
        body: "Unauthorised User"
      }
    };

    console.log(err.status+"no student")
    return callback(err, null);
}
  }
  else{
    err.status = 401;
    err.data = {
      errors: {
        body: "Unauthorised User"
      }
    };
    return callback(err, null);
  }
};

exports.getVisitedStudent = getVisitedStudent
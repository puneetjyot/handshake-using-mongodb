const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
const { Company, Event } = require("../../db/comapnymodel");
const { StudentEvents } = require("../../db/eventmodel");
let getStudentByEvent = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("----------getting all students");
  Decryptedtoken = decryptToken(msg.auth);
  try {
    await Company.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        if (tokenuser) {
          console.log(
            tokenuser.company_basic_detail_id +
              "in details ------------------------"
          );
          studentId = tokenuser.company_basic_detail_id;
          email = tokenuser.emailId;
          name = tokenuser.company_name;
        } else {
          err.status = 403;
          err.data = {
            errors: {
              body: "Unauthorised user"
            }
          };
          return callback(err, null);
        }
      })
      .catch(err => {
        console.log(`getting students who applied for this event ${err}`);
      });
    studentIdarr = [];
    console.log("event id" + msg.id);
    StudentEvents.find({
      event_detail_id: msg.id
    }).then(async tokenuser => {
      console.log(tokenuser);
      tokenuser.map(e => studentIdarr.push(e.student_basic_detail_id));
      var aggregate = Student.aggregate([
        { $unwind: "$educations" },
        { $match: { "educations.isPrimary": "1" }},
       { $match: {  student_basic_detail_id: { $in: studentIdarr } }
      }
      ]);
      await Student.aggregatePaginate(aggregate).then(tokenuser1 => {
        let eventData = {
          success: true,
          msg: "Successfully fetched student registered for events",
          msgDesc: tokenuser1.docs
        };
        (response.data = eventData), (response.status = 201);
        return callback(null, response);
      });
    });
  } catch (error) {
    console.log(`error getting jobs ${error}`);
    err.status = 403;
    err.data = {
      errors: {
        body: error
      }
    };
    return callback(err, null);
  }
};

exports.getStudentByEvent = getStudentByEvent;

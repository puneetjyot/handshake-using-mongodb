const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
const { Company, Event } = require("../../db/comapnymodel");
const { StudentEvents } = require("../../db/eventmodel");
let getRegisteredEvents = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("----------getting registered events");
  var studentId;
  Decryptedtoken = decryptToken(msg.auth);
  try {
    await Student.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        console.log(
          tokenuser.student_basic_detail_id +
            "in details ------------------------"
        );
        studentId = tokenuser.student_basic_detail_id;
        email = tokenuser.emailId;
        name = tokenuser.name;
      })
      .catch(err => {
        console.log(`error posting student journey ${err}`);
      });
    console.log(studentId);
    const result = await StudentEvents.find({
      student_basic_detail_id: studentId
    });

    let eventIdArr = [];
    result.map(i => {
      eventIdArr.push(i.event_detail_id);
    });
    console.log(eventIdArr);

    const finalresult = await Event.find({
      _id: { $in: eventIdArr }
    });

    console.log("sending jobs-----------------" + finalresult);
    (response.data = finalresult), (response.status = 201);
    return callback(null, response);
  } catch (error) {
    console.log(`error getting events ${err}`);
    err.status = 403;
    err.data = {
      errors: {
        body: error
      }
    };
    return callback(err, null);
  }
};

exports.getRegisteredEvents = getRegisteredEvents;

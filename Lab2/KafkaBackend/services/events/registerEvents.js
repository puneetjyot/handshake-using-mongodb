const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
const { Company, Event } = require("../../db/comapnymodel");
const {StudentEvents } = require("../../db/eventmodel");
let registerEvents = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("registering for event");
  var studentId;
  var student;
  var educationarr;
  Decryptedtoken = decryptToken(msg.auth);
  try {
    await Student.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        if (tokenuser) {
          console.log(tokenuser.student_basic_detail_id + "in details");
          student = tokenuser;
          studentId = tokenuser.student_basic_detail_id;
          email = tokenuser.emailId;
          name = tokenuser.name;
          educationarr = tokenuser.educations;
        } else {
          err.status = 403;
          err.data = {
            errors: {
              body: "Unauthenticated user"
            }
          };
          return callback(err, null);
        }
      })
      .catch(err => {
        console.log(`error getting student basic details ${err}`);
      });

    //getting major of current college

    var mainEducation = educationarr.filter(e => e.isPrimary == "1");

    if (
      msg.event.major.toLowerCase().includes("all") ||
      msg.event.major.toLowerCase().includes(mainEducation.major.toLowerCase())
    ) {
      const result = await StudentEvents.create({
        event_detail_id: msg.event.event_id,
        student_basic_detail_id: student.student_basic_detail_id
      });
      if (result) {
        (response.data = result), (response.status = 201);
        return callback(null, response);
      }
    } else {
      res.status(403).send({
        eligible: "Not Eligible"
      });
      err.status = 403;
      err.data = {
        eligible: "Not Eligible"
      };
      return callback(err, null);
    }
  } catch (error) {
    console.log(error);
    err.status = 403;
    err.data = {
        error:error.name
    };
    return callback(err, null);
    //res.status(403).send(error.name);
  }
};

exports.registerEvents = registerEvents;

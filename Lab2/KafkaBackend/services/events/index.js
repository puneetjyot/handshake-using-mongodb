"use strict";
const { getEvents } = require("./getEvents");
const { getRegisteredEvents } = require("./getRegisteredEvents");
const { getStudentByEvent } = require("./getStudentByEvent");
const { isRegistered } = require("./isRegistered");
const { postEvents } = require("./postEvents");
const { registerEvents } = require("./registerEvents");

let handle_request = (msg, callback) => {
  switch (msg.route) {
    case "get_events":
      getEvents(msg, callback);
      break;
      case "get_registered_events":
      getRegisteredEvents(msg, callback);
      break;
      case "get_student_by_event":
      getStudentByEvent(msg, callback);
      break;
      case "is_registered":
      isRegistered(msg, callback);
      break;
      case "post_events":
      postEvents(msg, callback);
      break;
      case "register_events":
      registerEvents(msg, callback);
      break;
   
  }
};

exports.handle_request = handle_request;

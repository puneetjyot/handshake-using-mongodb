const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
const { Company, Event } = require("../../db/comapnymodel");
let getEvents = async (msg, callback) => {
  let response = {};
  let err = {};

  console.log("----------getting jobs")
  Decryptedtoken = decryptToken(msg.auth);
  try {
    var {page,limit}=msg.query;
    console.log(parseInt(page,10))
    var options={
      page:parseInt(page,10)||1,
      limit:parseInt(limit,10)||10,
      sort:{
        date:1
      }
    }
    await Student
      .findOne({
      
          emailId: Decryptedtoken.email
       
      })
      .then(tokenuser => {
       
        console.log(
          tokenuser.student_basic_detail_id + "in details ------------------------"
        );
        studentId = tokenuser.student_basic_detail_id;
        email = tokenuser.emailId;
        name= tokenuser.name;
        
       
      })
      .catch(err =>{
        console.log(`error posting student journey ${err}`)
      });
      var whereCondition={}
      if (msg.locationAndTitleFilter !== "empty") {
        console.log("inside if clause for company_fiiilter");
        whereCondition = {
          ...whereCondition,
          $or: [
            {
              location: { $regex: new RegExp(msg.locationAndTitleFilter, "i") }
            },
            { event_name: { $regex: new RegExp(msg.locationAndTitleFilter, "i") } }
          ]
        };
      }
  
    console.log(whereCondition)
      const result = await Event.paginate(
       whereCondition,options
      
      )
      console.log("sending jobs-----------------"+result)
     let eventData={
        
            result:result.docs,
            total:result.total
          
     };
     (response.data = eventData), (response.status = 201);
     return callback(null, response);
}
catch(error)
{
  console.log(`error getting jobs ${error}`)
  err.status = 403;
    err.data = {
      errors: {
        body: error
      }
    };
    return callback(err, null);
}
  
};

exports.getEvents = getEvents;

const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
const { Company, Event } = require("../../db/comapnymodel");
const { StudentEvents } = require("../../db/eventmodel");
let isRegistered = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("----------getting registered events")
  Decryptedtoken = decryptToken(msg.auth);
  try {
    var studentId;
    await Student
      .findOne({
       
          emailId: Decryptedtoken.email
        
      })
      .then(tokenuser => {
        if(tokenuser){
        console.log(
          tokenuser.student_basic_detail_id + "in details ------------------------"
        );
        studentId = tokenuser.student_basic_detail_id;
        email = tokenuser.emailId;
        name= tokenuser.name;
        }
        else{
            err.status = 403;
            err.data = {
              errors: {
                body: error
              }
            };
            return callback(err, null);
        }
      })
      .catch(err =>{
        console.log(`error posting student journey ${err}`)
      });
    
      const result = await StudentEvents.findOne({

       
            student_basic_detail_id:studentId,
            event_detail_id:msg.event.event_id
              
    
      }
      )
      if(result)
      {
  
    let eventData={
        result:{
            registered:true
        } 
     };
      (response.data = eventData), (response.status = 201);
      return callback(null, response);
      }
      else{
          console.log("sending jobs-----------------"+result)
         
         let eventData={
            result:{
                registered:false
            } 
         };
          (response.data = eventData), (response.status = 201);
          return callback(null, response);
         
      }
      
}
catch(error)
{
  console.log(`error getting events ${error}`)
  err.status = 500;
    err.data = {
      errors: {
        body: error
      }
    };
    return callback(err, null);
}
  
};

exports.isRegistered = isRegistered;

const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
const { Company, Event } = require("../../db/comapnymodel");
let postEvents = async (msg, callback) => {
  let response = {};
  let err = {};
  Decryptedtoken = decryptToken(msg.auth);
  try {
    var companyId;
    await Company.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        if (tokenuser) {
          console.log(
            tokenuser.company_basic_detail_id +
              "in details ------------------------"
          );
          companyId = tokenuser.company_basic_detail_id;
          email = tokenuser.emailId;
          name = tokenuser.company_name;
        } else {
          err.status = 403;
          err.data = {
            errors: {
              body: error
            }
          };
          return callback(err, null);
        }
      })
      .catch(err => {
        console.log(`posting events ${err}`);
      });
      console.log(msg.event)
    const result = await Event.create({
      event_name: msg.event.event_name,
      event_time: msg.event.event_time,
      location: msg.event.location,
      eligibility: msg.event.eligibility,
      date: msg.event.date,
      event_description: msg.event.event_description,
      company_basic_detail_id: companyId,
      company_name: msg.event.company_name
    });

    if (result) {
      var { page, limit } = msg.query;
      console.log(parseInt(page, 10));
      var options = {
        page: parseInt(page, 10) || 1,
        limit: parseInt(limit, 10) || 10
        
      };
      const eventarr = await Event.paginate({},options);
      (response.data = {eventarr}), (response.status = 201);
      return callback(null, response);
    } else {
      err.status = 403;
      err.data = {
        errors: {
          body: error
        }
      };
      return callback(err, null);
    }
  } catch (err) {
    console.log(err);
    err.status = 403;
    err.data = {
      errors: {
        body: error
      }
    };
    return callback(err, null);
  }
};

exports.postEvents = postEvents;

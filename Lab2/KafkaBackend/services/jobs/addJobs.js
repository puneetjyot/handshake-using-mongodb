const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
const { Company, Job } = require("../../db/comapnymodel");
let addJobs = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("----------adding jobs");
  var companyId, name;
  Decryptedtoken = decryptToken(msg.auth);
  try {
    await Company.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        if (tokenuser) {
          companyId = tokenuser.company_basic_detail_id;
          email = tokenuser.emailId;
          name = tokenuser.company_name;
        } else {
          err.status = 403;
          err.data = {
            errors: {
              message: "unauthenticated user"
            }
          };
          return callback(err, null);
        }
      })
      .catch(err => {
        console.log(`error posting jobs ${err}`);
      });

    const result = await Job.create({
      job_title: msg.job.job_title,
      deadline: msg.job.deadline,
      location: msg.job.location,
      salary: msg.job.salary,
      job_description: msg.job.job_description,
      job_category: msg.job.job_category,
      company_basic_detail_id: companyId,
      company_name: name
    });

    if (result) {
      var { page, limit } = msg.query;
      console.log(parseInt(page, 10));
      var options = {
        page: parseInt(page, 10) || 1,
        limit: parseInt(limit, 10) || 10
        
      };

      const jobArr = await Job.paginate({}, options);

      (response.data = jobArr), (response.status = 201);
      return callback(null, response);
    } else {
      err.status = 403;
      err.data = {
        errors: {
          message: "unable to add jobs"
        }
      };
      return callback(err, null);
    }
  } catch (error) {
    console.log(error);
    err.status = 403;
    err.data = {
      errors: {
        message: error
      }
    };
    return callback(err, null);
  }
};

exports.addJobs = addJobs;

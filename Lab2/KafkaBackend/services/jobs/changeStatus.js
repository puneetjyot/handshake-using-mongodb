const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
const { Company, Job } = require("../../db/comapnymodel");
const { StudentJobs } = require("../../db/jobmodel");
let changeStatus = async (msg, callback) => {
  let response = {};
  let err = {};
  try {
   
    Decryptedtoken = decryptToken(msg.auth);
    await Company.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        if (tokenuser) {
          console.log(
            tokenuser.company_basic_detail_id +
              "in details ------------------------"
          );
          companyId = tokenuser.company_basic_detail_id;
          email = tokenuser.emailId;
          name = tokenuser.company_name;
        } else {
            err.status = 403;
            err.data = {
              errors: {
                err: "unauthenticated user"
              }
            };
            return callback(err, null);
        }
      })
      .catch(err => {
        console.log(`updating studentsstatus for job ${err}`);
      });

    const filter = {
      job_id: msg.params.jobId,
      student_basic_detail_id: msg.params.studentId
    };
    const update = { status: msg.company.status };

    await StudentJobs.findOneAndUpdate(filter, update, {
      new: true,
      useFindAndModify: true
    })
      .then(tokenuser => {
        if (tokenuser) {
            (response.data = tokenuser), (response.status = 201);
            return callback(null, response);
        }
        else
        err.status = 500;
        err.data = {
          errors: {
            body: "error while updating status"
          }
        };
        return callback(err, null);
      })
      .catch(err => {
        console.log(err);
        err.status = 500;
        err.data = {
          errors: {
            body: "error while updating status"
          }
        };
        return callback(err, null);
      });
  } catch (error) {
    console.log(`error getting jobs ${err}`);
    err.status = 500;
    err.data = {
      errors: {
        body: error
      }
    };
    return callback(err, null);
  }
};

exports.changeStatus = changeStatus;

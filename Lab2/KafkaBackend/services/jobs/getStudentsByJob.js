const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
const { Company, Job } = require("../../db/comapnymodel");
const { StudentJobs } = require("../../db/jobmodel");
let getStudentByJob = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("----------getting all students");
  Decryptedtoken = decryptToken(msg.auth);
  try {
    await Company.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        if (tokenuser) {
          studentId = tokenuser.company_basic_detail_id;
          email = tokenuser.emailId;
          name = tokenuser.company_name;
        } else {
         err.status = 403;
        err.data = {
          errors: {
            err: "unauthenticated user"
          }
        };
        return callback(err, null);
        }
      })
      .catch(err => {
        console.log(`getting students who applied for this job ${err}`);
      });
    var finalarray = [];
    await StudentJobs.find({
      job_id: msg.id
    })
      .populate("student_id")
      .then(tokenuser => {
        if (tokenuser) {
          // console.log(tokenuser)
          tokenuser.map(t => {
            finalusereducation = t.student_id.educations.filter(
              e => e.isPrimary == 1
            );
            // console.log(finalusereducation)
            t.student_id.educations = finalusereducation;
           // console.log(t);
            finalarray.push(t);
          });
         // console.log(finalarray);
        }
       let result={
          success: true,
          msg: "Successfully fetched student profile",
          msgDesc: finalarray
        };
        (response.data = result), (response.status = 201);
        return callback(null, response);
       
      });
  } catch (err) {
    console.log(`error getting jobs ${err}`);
   err.status = 500;
        err.data = {
          errors: {
            body: err
          }
        };
        return callback(err, null);
  }
};

exports.getStudentByJob = getStudentByJob;

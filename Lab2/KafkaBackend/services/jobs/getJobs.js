const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
const { Company, Job } = require("../../db/comapnymodel");
let getJobs = async (msg, callback) => {
  let response = {};
  let err = {};
  console.log("----------getting jobs");
  Decryptedtoken = decryptToken(msg.auth);
  var sort = "";
  var whereCondition = {};
  if (msg.categoryFilter !== "empty") {
    whereCondition = {
      ...whereCondition,
      job_category: msg.categoryFilter
    };
  }
  if (msg.companyFilter !== "empty") {
    console.log("inside if clause for company_fiiilter");
    whereCondition = {
      ...whereCondition,
      $or: [
        {
          company_name: { $regex: new RegExp(msg.companyFilter, "i") }
        },
        { job_title: { $regex: new RegExp(msg.companyFilter, "i") } }
      ]
    };
  }

  if (msg.locationFilter !== "empty") {
    whereCondition = {
      ...whereCondition,
      location: { $regex: new RegExp(msg.locationFilter, "i") }
    };
  }
  if (msg.sortFilter !== "empty") {
    console.log("inside sorting" + msg.sortFilter);
    console.log(sort);
    switch (msg.sortFilter) {
      case "DeadlineAsc": {
        sort = {
          deadline: 1
        };
        break;
      }
      case "DeadlineDesc": {
        console.log("in descending");
        sort = "";
        sort = {
          deadline: -1
        };
        break;
      }
      case "LocationAsc": {
        console.log("in location");
        sort = {
          location: 1
        };
        break;
      }
      case "LocationDesc": {
        console.log("in location");
        sort = {
          location: -1
        };
        break;
      }
      case "PostedonAsc": {
        console.log("in posting");
        sort = {
          createdAt: 1
        };
        break;
      }
      case "PostedonDesc": {
        console.log("in posting");
        sort = {
          createdAt: -1
        };
        break;
      }
      default: {
        sort = {
          deadline: 1
        };
        break;
      }
    }
  }
  console.log(sort);

  try {
    await Student.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        console.log(
          tokenuser.student_basic_detail_id +
            "in details ------------------------"
        );
        studentId = tokenuser.student_basic_detail_id;
        email = tokenuser.emailId;
        name = tokenuser.name;
      })
      .catch(err => {
        console.log(`error posting student journey ${err}`);
      });
    var { page, limit } = msg.query;
    console.log(parseInt(page, 10));
    var options = {
      page: parseInt(page, 10) || 1,
      limit: parseInt(limit, 10) || 10,
      sort: sort
    };

    const result = await Job.paginate(whereCondition, options);
    console.log("sending jobs-----------------" + result);

    // res.status(201).send({
    //   result: result.docs,
    //   total:result.total,
    //   pages:result.pages
    // });

    let jobdata = {
      result: result.docs,
      total: result.total,
      pages: result.pages
    };
    (response.data = jobdata), (response.status = 201);
    return callback(null, response);
  } catch (error) {
    console.log(`error getting jobs ${error}`);
    err.status = 403;
    err.data = {
      errors: {
        message: error
      }
    };
    return callback(err, null);
  }
};

exports.getJobs = getJobs;

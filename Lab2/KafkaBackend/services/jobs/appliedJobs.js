const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
const { Company, Job } = require("../../db/comapnymodel");
const { StudentJobs } = require("../../db/jobmodel");
let appliedJobs = async (msg, callback) => {
  let response = {};
  let err = {};
 
  console.log("----------getting applied jobs");
  Decryptedtoken = decryptToken(msg.auth);
  var studentId;
  var { page, limit } = msg.query;
  console.log(parseInt(page, 10));
  var options = {
    page: parseInt(page, 10) || 1,
    limit: parseInt(limit, 10) || 10,
    populate: "job_id"
  };
  try {
    var whereCondition = {};
    if (msg.statusFilter !== "empty") {
      whereCondition = {
        ...whereCondition,
        status: msg.statusFilter
      };
    }

    await Student.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        console.log(
          tokenuser.student_basic_detail_id +
            "in details ------------------------"
        );
        studentId = tokenuser.student_basic_detail_id;
        email = tokenuser.emailId;
        name = tokenuser.name;
      })
      .catch(err => {
        console.log(`applying for jobs ${err}`);
      });

    const jobsAppliedArr = await StudentJobs.paginate(
      {
        student_basic_detail_id: studentId,
        ...whereCondition
      },
      options
    )
    .then(finalarray => {
      console.log("sending jobs-----------------" + finalarray);
      let jobdata={
        result: finalarray.docs,
        total: finalarray.total
      };
      (response.data = jobdata), (response.status = 201);
      return callback(null, response);
    });
  } catch (err) {
    console.log(`error getting jobs ${err}`);
    err.status = 403;
        err.data = {
          errors: {
            body: "unable to get applied jobs"
          }
        };
        return callback(err, null);
  }


};

exports.appliedJobs = appliedJobs;

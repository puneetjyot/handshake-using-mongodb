"use strict";
const { getJobs } = require("./getJobs");
const { addJobs } = require("./addJobs");
const { appliedJobs } = require("./appliedJobs");
const { applyJobs } = require("./applyJobs");
const { getStudentByJob } = require("./getStudentsByJob");
const { changeStatus } = require("./changeStatus");

let handle_request = (msg, callback) => {
  switch (msg.route) {
    case "get_jobs":
      getJobs(msg, callback);
      break;
    case "add_jobs":
      addJobs(msg, callback);
      break;
    case "applied_jobs":
      appliedJobs(msg, callback);
      break;

    case "apply_jobs":
      applyJobs(msg, callback);
      break;

    case "get_Student_By_jobs":
      getStudentByJob(msg, callback);
      break;

    case "change_status":
      changeStatus(msg, callback);
      break;

  }
};

exports.handle_request = handle_request;

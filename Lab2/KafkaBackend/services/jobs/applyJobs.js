const { Student } = require("../../db/studentmodel");
const { generateToken, decryptToken } = require("../../service/tokenservice");
const { Company, Job } = require("../../db/comapnymodel");
const { StudentJobs } = require("../../db/jobmodel");
let applyJobs = async (msg, callback) => {
  let response = {};
  let err = {};
 console.log("applying job in kafka backend")
  var studentId;
  var studentObjectId;
  var student;
  Decryptedtoken = decryptToken(msg.auth);
  try {
    await Student.findOne({
      emailId: Decryptedtoken.email
    })
      .then(tokenuser => {
        student = tokenuser;
        studentId = tokenuser.student_basic_detail_id;
        studentObjectId = tokenuser._id;
      })
      .catch(err => {
        console.log(`error getting student basic details ${err}`);
      });

    // console.log(student, "-----------------------------------", bookId);
    const result = await StudentJobs.create({
      job_id: msg.params.id,
      status: "Pending",
      student_basic_detail_id: student.student_basic_detail_id,
      resume: msg.file ? msg.file.originalname : "",
      student_id: studentObjectId
    });
    if (result) {
      (response.data = result), (response.status = 201);
        return callback(null, response);
    }
  } catch (error) {
    err.status = 403;
        err.data = {
          errors: {
            message: error
          }
        };
        return callback(err, null);
  }
};

exports.applyJobs = applyJobs;

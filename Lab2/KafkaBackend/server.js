var connection = new require("./kafka/Connection");

const mongoose = require("mongoose");
var options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  reconnectInterval: 500,
  poolSize: 50,
  bufferMaxEntries: 0
};

mongoose.connect(
  "mongodb+srv://puneetjyot:handshake@handshake-mongo-zu6wh.mongodb.net/Handshake?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
);
const accountService = require("./services/account");
const searchService = require("./services/search");
const studentProfileService = require("./services/studentprofile");
const studentSkillService = require("./services/studentskills");
const studentBasicDetailService = require("./services/basicdetails");
const studentExperienceService = require("./services/studentexperience");
const jobsService = require("./services/jobs");
const eventsService = require("./services/events");
const companyService = require("./services/company");
const messageService = require("./services/messages");

function handleTopicRequest(topic_name, fname) {
  //var topic_name = 'root_topic';

  var consumer = connection.getConsumer(topic_name);
  var producer = connection.getProducer();
  console.log("server is running ");
  consumer.on("message", function(message) {
    console.log("message received for " + topic_name + " ", fname);
    console.log(JSON.stringify(message.value));
    var data = JSON.parse(message.value);

    fname.handle_request(data.data, function(err, res) {
      var result = "";
      console.log("after handle" + JSON.stringify(err));
      if (err) {
        result = err;
      } else {
        result = res;
      }
      var payloads = [
        {
          topic: data.replyTo,
          messages: JSON.stringify({
            correlationId: data.correlationId,
            data: result
          }),
          partition: 0
        }
      ];
      producer.send(payloads, function(err, data) {
        if (err) {
          console.log("Error when producer sending data", err);
        } else {
          console.log(data);
        }
      });
      return;
    });
  });
}
// Add your TOPICs here
//first argument is topic name
//second argument is a function that will handle this topic request
handleTopicRequest("account", accountService);
handleTopicRequest("search", searchService);
handleTopicRequest("student_profile", studentProfileService);
handleTopicRequest("student_skill", studentSkillService);
handleTopicRequest("student_basic_detail", studentBasicDetailService);
handleTopicRequest("student_experience", studentExperienceService);
handleTopicRequest("jobs", jobsService);
handleTopicRequest("events", eventsService);
handleTopicRequest("company", companyService);
handleTopicRequest("messages", messageService);

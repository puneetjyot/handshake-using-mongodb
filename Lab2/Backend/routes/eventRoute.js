var express = require("express");
var route = express.Router();
const { generateToken, decryptToken } = require("../service/tokenservice");
const { generateUUID } = require("../service/uuidservice");
const passport = require("../authenticate/passport_init");
const key = require("../service/key");
const { Company, Event } = require("../db/comapnymodel");
const { StudentEvents } = require("../db/eventmodel");
const { Student } = require("../db/studentmodel");
const kafka = require("../kafka/client");

const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

route.get("/:locationAndTitleFilter", async (req, res) => {
  let msg = {};
  (msg = req.params), (msg.query = req.query);
  msg.auth = req.headers.authorization;
  msg.route = "get_events";

  kafka.make_request("events", msg, function(err, results) {
    if (err) {
      console.log("in error");
      msg.error = err.data;
      return res.status(err.status).send(err.data);
    } else {
      console.log("in success");
      msg.status = results.status;
      return res.status(results.status).send(results.data);
    }
  });
});

route.post("/register", async (req, res, next) => {
  let msg = req.body;

  msg.auth = req.headers.authorization;
  msg.route = "register_events";

  kafka.make_request("events", msg, function(err, results) {
    if (err) {
      console.log("in error");
      msg.error = err.data;
      return res.status(err.status).send(err.data);
    } else {
      console.log("in success");
      msg.status = results.status;
      return res.status(results.status).send(results.data);
    }
  });
});

route.get("/registered/student", async (req, res) => {
  let msg = req.body;

  msg.auth = req.headers.authorization;
  msg.route = "get_registered_events";

  kafka.make_request("events", msg, function(err, results) {
    if (err) {
      console.log("in error");
      msg.error = err.data;
      return res.status(err.status).send(err.data);
    } else {
      console.log("in success");
      msg.status = results.status;
      return res.status(results.status).send(results.data);
    }
  });
});

route.post("/isregistered", async (req, res) => {
  let msg = {};
  (msg = req.body), (msg.auth = req.headers.authorization);
  msg.route = "is_registered";

  kafka.make_request("events", msg, function(err, results) {
    if (err) {
      console.log("in error");
      msg.error = err.data;
      return res.status(err.status).send(err.data);
    } else {
      console.log("in success");
      msg.status = results.status;
      return res.status(results.status).send(results.data);
    }
  });
});

route.get("/:id/students", async (req, res) => {
  let msg = {};
  (msg = req.params), (msg.auth = req.headers.authorization);

  msg.route = "get_student_by_event";

  kafka.make_request("events", msg, function(err, results) {
    if (err) {
      console.log("in error");
      msg.error = err.data;
      return res.status(err.status).send(err.data);
    } else {
      console.log("in success");
      msg.status = results.status;
      return res.status(results.status).send(results.data);
    }
  });
});

route.post("/", async (req, res) => {
  let msg = {};
  (msg = req.body), (msg.auth = req.headers.authorization);
  msg.query=req.query;
  msg.route = "post_events";

  kafka.make_request("events", msg, function(err, results) {
    if (err) {
      console.log("in error");
      msg.error = err.data;
      return res.status(err.status).send(err.data);
    } else {
      console.log("in success");
      msg.status = results.status;
      return res.status(results.status).send(results.data);
    }
  });
});

module.exports = route;

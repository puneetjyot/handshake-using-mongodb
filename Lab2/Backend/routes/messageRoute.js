var express = require("express");
var route = express.Router();
const kafka = require("../kafka/client");

route.get(
    "/:id",
    async (req, res) => {
      console.log("in getting messages")
      console.log(req.params.id)
      let msg = {}
      msg=req.params
      msg.auth=req.headers.authorization
      msg.route = "get_messages";
     
      kafka.make_request("messages", msg, function(err, results) {
        if (err) {
          console.log("in error");
          msg.error = err.data;
          return res.status(err.status).send(err.data);
        } else {
          console.log("in success");
          msg.status = results.status;
          return res.status(results.status).send(results.data);
        }
      });
    }
  );
  route.post(
    "/:senderId/:receiverId/:senderModel/:receiverModel",
    async (req, res) => {
      console.log("in getting messages")
      let msg = {}
      msg=req.body
      msg.params=req.params
      msg.auth=req.headers.authorization
      msg.route = "send_message";
     
      kafka.make_request("messages", msg, function(err, results) {
        if (err) {
          console.log("in error");
          msg.error = err.data;
          return res.status(err.status).send(err.data);
        } else {
          console.log("in success");
          msg.status = results.status;
          return res.status(results.status).send(results.data);
        }
      });
    }
  );
  module.exports = route;
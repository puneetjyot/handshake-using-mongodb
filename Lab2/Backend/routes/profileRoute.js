var express = require("express");
var route = express.Router();
const { generateToken, decryptToken } = require("../service/tokenservice");
const { generateUUID } = require("../service/uuidservice");
const passport = require("../authenticate/passport_init");
const key = require("../service/key");
const kafka = require("../kafka/client");
const {
  validateUsername,
  validatePassword,
  validateEmail
} = require("../companymiddleware");

const jwt = require("jsonwebtoken");
var connection = require("../db_connection");
const bcrypt = require("bcrypt");
var multer = require("multer");

const { Student } = require("../db/studentmodel");

route.get(
  "/:studentnameFilter/:majorFilter/:skillFilter/:collegeFilter",
  async (req, res) => {
    let msg = {};
    msg=req.params,
    msg.query=req.query,
    msg.auth = req.headers.authorization;
    msg.route = "get_all_students";
   
  
    kafka.make_request("search", msg, function(err, results) {
      if (err) {
        console.log("in error");
        msg.error = err.data;
        return res.status(err.status).send(err.data);
      } else {
        console.log("in success");
        msg.status = results.status;
        return res.status(results.status).send(results.data);
      }
    });
  }
);

route.get("/education/:id", async (req, res) => {
  console.log(req.body);
  console.log("In get education");
  var studentId, student;
  Decryptedtoken = decryptToken(req.headers.authorization);
  try {
    await student_basic_details
      .findOne({
        where: {
          emailId: Decryptedtoken.email
        }
      })
      .then(tokenuser => {
        console.log(
          tokenuser.dataValues.student_basic_detail_id + "in details"
        );
        studentId = tokenuser.dataValues.student_basic_detail_id;
        email = tokenuser.dataValues.emailId;
        name = tokenuser.dataValues.name;
      })
      .catch(err => {
        console.log(`error getting student basic details ${err}`);
      });

    const preeducation = await student_education.findAll({
      where: {
        student_basic_detail_id: req.params.id
      }
    });

    if (preeducation) {
      res.status(201).send(preeducation);
    } else {
      res.status(403).send({
        errors: {
          err: "Unable to delete school"
        }
      });
    }
  } catch (err) {
    console.log(err + "error sdsad");
    res.status(500).send({
      errors: {
        body: "cannot delete as record is not present"
      }
    });
  }
});

route.get("/skills/:id", async (req, res) => {
  console.log(req.body);
  console.log("In get education");
  var studentId;
  Decryptedtoken = decryptToken(req.headers.authorization);
  try {
    await student_basic_details
      .findOne({
        where: {
          emailId: Decryptedtoken.email
        }
      })
      .then(tokenuser => {
        console.log(
          tokenuser.dataValues.student_basic_detail_id + "in details"
        );
        studentId = tokenuser.dataValues.student_basic_detail_id;
        email = tokenuser.dataValues.emailId;
        name = tokenuser.dataValues.name;
      })
      .catch(err => {
        console.log(`error getting student basic details ${err}`);
      });

    const preeducation = await student_skills.findAll({
      where: {
        student_basic_detail_id: req.params.id
      }
    });

    if (preeducation) {
      res.status(201).send(preeducation);
    } else {
      res.status(403).send({
        errors: {
          err: "Unable to get skills"
        }
      });
    }
  } catch (err) {
    console.log(err + "error sdsad");
    res.status(500).send({
      errors: {
        body: "cannot find key as record is not present"
      }
    });
  }
});

route.get("/journey/:id", async (req, res) => {
  console.log("----------getting journey");
  Decryptedtoken = decryptToken(req.headers.authorization);
  try {
    await student_basic_details
      .findOne({
        where: {
          emailId: Decryptedtoken.email
        }
      })
      .then(tokenuser => {
        console.log(
          tokenuser.dataValues.student_basic_detail_id +
            "in details ------------------------"
        );
        studentId = tokenuser.dataValues.student_basic_detail_id;
        email = tokenuser.dataValues.emailId;
        name = tokenuser.dataValues.name;
      })
      .catch(err => {
        console.log(`error posting student journey ${err}`);
      });

    const result = await student_profile.findOne({
      where: { student_basic_detail_id: req.params.id }
    });
    console.log("sending journey-----------------" + result.career_objective);
    res.status(201).send({
      result: result.career_objective
    });
  } catch (err) {
    console.log(`error posting student journey ${err}`);
    res.status(500).send({
      errors: {
        body: err
      }
    });
  }
});

route.get("/experience/:id", async (req, res) => {
  try {
    const experiencearr = await student_experience.findAll({
      where: {
        student_basic_detail_id: req.params.id
      }
    });

    if (experiencearr) {
      res.status(201).send({
        experiencearr
      });
    }
  } catch (err) {
    res.status(403).send({
      errors: {
        err: "Unable to delete school"
      }
    });
  }
});

module.exports = route;

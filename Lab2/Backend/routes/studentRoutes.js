var express = require("express");
var route = express.Router();
const { generateToken, decryptToken } = require("../service/tokenservice");
const { generateUUID } = require("../service/uuidservice");
//const passport = require("../authenticate/passport_init");
const key = require("../service/key");
const mongoose = require("mongoose");
var multer = require("multer");
const passport = require("passport");
const {
  validateUsername,
  validatePassword,
  validateEmail
} = require("../studentmiddleware");
const kafka = require("../kafka/client");
const { Student } = require("../db/studentmodel");
const { StudentJobs } = require("../db/jobmodel");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "public");
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname);
  }
});

var upload = multer({ storage: storage });

route.get("/servercheck", (req, res) => {
  res.status(200).send("Welcome to Handshake server");
});

route.get("/", async (req, res) => {
  let msg = {};
  msg.auth = req.headers.authorization;
  msg.route = "get_student";
  kafka.make_request("search", msg, function(err, results) {
    if (err) {
      console.log("in error");
      msg.error = err.data;
      return res.status(err.status).send(err.data);
    } else {
      console.log("in success");
      msg.status = results.status;
      return res.status(results.status).send(results.data);
    }
  });
});

route.get("/visit/:id", async (req, res) => {
  let msg = {};
  msg.auth = req.headers.authorization;
  msg.id = req.params.id;
  msg.route = "get_visitedstudent";
  kafka.make_request("search", msg, function(err, results) {
    if (err) {
      console.log("in error");
      msg.error = err.data;
      return res.status(err.status).send(err.data);
    } else {
      console.log("in success");
      msg.status = results.status;
      return res.status(results.status).send(results.data);
    }
  });

  // }
});

route.post(
  "/register",
  validateUsername,
  validatePassword,
  validateEmail,
  async (req, res) => {
    let msg = req.body;
    msg.route = "register_student";

    kafka.make_request("account", msg, function(err, results) {
      if (err) {
        msg.error = err.data;

        return res.status(err.status).send(err.data);
      } else {
        msg.status = results.status;

        return res.status(results.status).send(results.data);
      }
    });
  }
);

route.post("/journey", async (req, res) => {
  let msg = req.body;
  msg.auth = req.headers.authorization;
  msg.route = "post_studentjourney";

  kafka.make_request("student_profile", msg, function(err, results) {
    if (err) {
      msg.error = err.data;

      return res.status(err.status).send(err.data);
    } else {
      msg.status = results.status;

      return res.status(results.status).send(results.data);
    }
  });
});
route.post("/login", validateEmail, validatePassword, async (req, res) => {
  let msg = req.body;
  msg.route = "login_student";

  kafka.make_request("account", msg, function(err, results) {
    if (err) {
      msg.error = err.data;

      return res.status(err.status).send(err.data);
    } else {
      msg.status = results.status;

      return res.status(results.status).send(results.data);
    }
  });
});

route.get("/journey", async (req, res) => {
  let msg = req.body;
  msg.auth = req.headers.authorization;
  msg.route = "get_studentjourney";

  kafka.make_request("student_profile", msg, function(err, results) {
    if (err) {
      msg.error = err.data;

      return res.status(err.status).send(err.data);
    } else {
      msg.status = results.status;

      return res.status(results.status).send(results.data);
    }
  });
});

route.put("/name", async (req, res) => {
  let msg = req.body;
  msg.auth = req.headers.authorization;
  msg.route = "edit_name";

  kafka.make_request("student_profile", msg, function(err, results) {
    if (err) {
      msg.error = err.data;

      return res.status(err.status).send(err.data);
    } else {
      msg.status = results.status;

      return res.status(results.status).send(results.data);
    }
  });
});

route.post("/education", async (req, res) => {
  console.log(req.body);

  let msg = req.body;
  msg.auth = req.headers.authorization;
  msg.route = "add_education";

  kafka.make_request("student_profile", msg, function(err, results) {
    if (err) {
      msg.error = err.data;

      return res.status(err.status).send(err.data);
    } else {
      msg.status = results.status;

      return res.status(results.status).send(results.data);
    }
  });
});

route.put("/education", async (req, res) => {
  console.log(req.body);
  let msg = req.body;
  msg.auth = req.headers.authorization;
  msg.route = "edit_education";

  kafka.make_request("student_profile", msg, function(err, results) {
    if (err) {
      msg.error = err.data;

      return res.status(err.status).send(err.data);
    } else {
      msg.status = results.status;

      return res.status(results.status).send(results.data);
    }
  });
});

route.delete("/education", async (req, res) => {
  console.log(req.body);
  let msg = req.body;
  msg.auth = req.headers.authorization;
  msg.route = "delete_education";

  kafka.make_request("student_profile", msg, function(err, results) {
    if (err) {
      msg.error = err.data;

      return res.status(err.status).send(err.data);
    } else {
      msg.status = results.status;

      return res.status(results.status).send(results.data);
    }
  });
});

// route.get("/education", async (req, res) => {
//   console.log(req.body);
//   console.log("In get education");
//   var studentId;
//   Decryptedtoken = decryptToken(req.headers.authorization);
//   try {
//     await Student.findOne({
//       emailId: Decryptedtoken.email
//     })
//       .then(tokenuser => {
//         res.status(201).send(tokenuser.educations);
//       })
//       .catch(err => {
//         console.log(`error getting student basic details ${err}`);
//       });
//   } catch (err) {
//     console.log(err + "error sdsad");
//     res.status(500).send({
//       errors: {
//         body: "cannot delete as record is not present"
//       }
//     });
//   }
// });

route.post("/picture", upload.single("myimage"), async (req, res) => {
  console.log(JSON.stringify(req.file) + " file post");
  let msg = req.file;
  msg.auth = req.headers.authorization;
  msg.route = "update_profilepic";

  kafka.make_request("student_profile", msg, function(err, results) {
    if (err) {
      msg.error = err.data;

      return res.status(err.status).send(err.data);
    } else {
      msg.status = results.status;

      return res.status(results.status).send(results.data);
    }
  });
});

// route.get("/picture", async (req, res) => {
//   var studentId;
//   Decryptedtoken = decryptToken(req.headers.authorization);
//   try {
//     await Student.findOne({
//       emailId: Decryptedtoken.email
//     })
//       .then(tokenuser => {
//         console.log(tokenuser.student_basic_detail_id + "in details");
//         studentId = tokenuser.student_basic_detail_id;
//         email = tokenuser.emailId;
//         name = tokenuser.name;
//       })
//       .catch(err => {
//         console.log(`error getting student basic details ${err}`);
//       });

//     Student.findOne({
//       student_basic_detail_id: studentId
//     })
//       .then(profile => {
//         res.json({ success: true, data: profile });
//       })
//       .catch(err => {
//         console.log("in error :: /api/getProfilePic");
//       });
//   } catch (err) {
//     console.log(err);
//     res.status(403).send({
//       errors: {
//         err: err
//       }
//     });
//   }
// });

route.post("/skills", async (req, res) => {
  console.log(req.body);

  let msg = req.body;
  msg.auth = req.headers.authorization;
  msg.route = "post_studentskill";

  kafka.make_request("student_skill", msg, function(err, results) {
    if (err) {
      msg.error = err.data;

      return res.status(err.status).send(err.data);
    } else {
      msg.status = results.status;

      return res.status(results.status).send(results.data);
    }
  });
});

///// Get for skills

// route.get("/skills", async (req, res) => {
//   console.log(req.body);
//   console.log("In get education");
//   var studentId;
//   Decryptedtoken = decryptToken(req.headers.authorization);
//   try {
//     await Student.findOne({
//       emailId: Decryptedtoken.email
//     })
//       .then(tokenuser => {
//         res.status(201).send(tokenuser.skills);
//       })
//       .catch(err => {
//         console.log(`error getting student basic details ${err}`);
//       });
//   } catch (err) {
//     console.log(err + "error sdsad");
//     res.status(500).send({
//       errors: {
//         body: "cannot find key as record is not present"
//       }
//     });
//   }
// });

route.delete("/skills", async (req, res) => {
  console.log();
  console.log("In deleting skill");
  let msg = req.body;
  msg.auth = req.headers.authorization;
  msg.route = "delete_studentskill";

  kafka.make_request("student_skill", msg, function(err, results) {
    if (err) {
      msg.error = err.data;

      return res.status(err.status).send(err.data);
    } else {
      msg.status = results.status;

      return res.status(results.status).send(results.data);
    }
  });
});

route.post("/basicdetails", async (req, res) => {
  console.log("In posting details");
  let msg = req.body;
  msg.auth = req.headers.authorization;
  msg.route = "post_basic_details";

  kafka.make_request("student_basic_detail", msg, function(err, results) {
    if (err) {
      msg.error = err.data;

      return res.status(err.status).send(err.data);
    } else {
      msg.status = results.status;

      return res.status(results.status).send(results.data);
    }
  });
});

route.post("/upload/:id", upload.single("myimage"), async (req, res) => {
  //console.log(req.file, "filee");
  console.log("applying for job");
  let msg = {};
  msg=req.body;
  msg.params = req.params;
  msg.file = req.file;
  msg.auth = req.headers.authorization;
  msg.route = "apply_jobs";

  kafka.make_request("jobs", msg, function(err, results) {
    if (err) {
      msg.error = err.data;

      return res.status(err.status).send(err.data);
    } else {
      msg.status = results.status;

      return res.status(results.status).send(results.data);
    }
  });
});

module.exports = route;

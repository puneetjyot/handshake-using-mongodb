var express = require("express");
var route = express.Router();
const { generateToken, decryptToken } = require("../service/tokenservice");
const { generateUUID } = require("../service/uuidservice");
const passport = require("../authenticate/passport_init");
const key = require("../service/key");
const {
  validateUsername,
  validatePassword,
  validateEmail
} = require("../companymiddleware");
const kafka = require("../kafka/client");
const { Company } = require("../db/comapnymodel");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
var multer = require("multer");

var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "public");
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname);
  }
});

var upload = multer({ storage: storage });

route.post(
  "/register",
  validateUsername,
  validatePassword,
  validateEmail,
  async (req, res) => {
    let msg=req.body
  msg.route="register_company"
  
  kafka.make_request("account", msg, function (err, results) {
    if (err) {
      msg.error = err.data;
      
      return res.status(err.status).send(err.data);
    }
    else{
      msg.status = results.status;
       
      return res.status(results.status).send(results.data);
    }
  })
  }
);

route.post("/login", validateEmail, validatePassword, async (req, res) => {
  let msg=req.body
  msg.route="login_company"
  
  kafka.make_request("account", msg, function (err, results) {
    if (err) {
      msg.error = err.data;
      
      return res.status(err.status).send(err.data);
    }
    else{
      msg.status = results.status;
       
      return res.status(results.status).send(results.data);
    }
  })
});

route.post("/details", async (req, res) => {
  let msg=req.body
  msg.route="post_details"
  
  kafka.make_request("company", msg, function (err, results) {
    if (err) {
      msg.error = err.data;
      
      return res.status(err.status).send(err.data);
    }
    else{
      msg.status = results.status;
       
      return res.status(results.status).send(results.data);
    }
  })
});

route.get("/", async (req, res) => {
  let msg=req.body
  msg.auth=req.headers.authorization
  msg.route="get_company"
  
  kafka.make_request("company", msg, function (err, results) {
    if (err) {
      msg.error = err.data;
      
      return res.status(err.status).send(err.data);
    }
    else{
      msg.status = results.status;
       
      return res.status(results.status).send(results.data);
    }
  })
});

route.put("/", async (req, res) => {
  let msg=req.body
  msg.auth=req.headers.authorization
  msg.route="update_details"
  
  kafka.make_request("company", msg, function (err, results) {
    if (err) {
      msg.error = err.data;
      
      return res.status(err.status).send(err.data);
    }
    else{
      msg.status = results.status;
       
      return res.status(results.status).send(results.data);
    }
  })
});

route.get("/:id", async (req, res) => {
  let msg=req.params
  msg.auth=req.headers.authorization
  msg.route="get_selected_company"
  
  kafka.make_request("company", msg, function (err, results) {
    if (err) {
      msg.error = err.data;
      
      return res.status(err.status).send(err.data);
    }
    else{
      msg.status = results.status;
       
      return res.status(results.status).send(results.data);
    }
  })
});

route.post("/picture", upload.single("myimage"), async (req, res) => {
  let msg=req.file
  msg.auth=req.headers.authorization
  msg.route="post_profile_picture"
  
  kafka.make_request("company", msg, function (err, results) {
    if (err) {
      msg.error = err.data;
      
      return res.status(err.status).send(err.data);
    }
    else{
      msg.status = results.status;
       
      return res.status(results.status).send(results.data);
    }
  })
});

module.exports = route;

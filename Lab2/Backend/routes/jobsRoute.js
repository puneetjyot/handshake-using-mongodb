var express = require("express");
var route = express.Router();
const { generateToken, decryptToken } = require("../service/tokenservice");
const { generateUUID } = require("../service/uuidservice");
const passport = require("../authenticate/passport_init");
const key = require("../service/key");
const fileUpload = require("express-fileupload");
const kafka = require("../kafka/client");
const { Student } = require("../db/studentmodel");
const { StudentJobs } = require("../db/jobmodel");
const { Company, Job } = require("../db/comapnymodel");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
var multer = require("multer");
route.use(fileUpload());

// var storage = multer.diskStorage({
//       destination: function (req, file, cb) {
//       cb(null, 'public')
//     },
//     filename: function (req, file, cb) {
//       cb(null, Date.now() + '-' +file.originalname )
//     }
// })

var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "public");
  },
  filename: function(req, file, cb) {
    cb(null, new Date() + "-" + file.originalname);
  }
});

var upload = multer({ storage: storage });

route.get(
  "/:companyFilter/:locationFilter/:categoryFilter/:sortFilter",
  async (req, res) => {
    console.log("in getting jobs")
    let msg = {};
    msg = req.params;
    msg.auth = req.headers.authorization;
    msg.route = "get_jobs";
    msg.query = req.query;
    kafka.make_request("jobs", msg, function(err, results) {
      if (err) {
        console.log("in error");
        msg.error = err.data;
        return res.status(err.status).send(err.data);
      } else {
        console.log("in success");
        msg.status = results.status;
        return res.status(results.status).send(results.data);
      }
    });
  }
);

route.post("/", async (req, res) => {
  let msg = {};
  msg = req.body;
  msg.query=req.query;
  msg.auth = req.headers.authorization;
  msg.route = "add_jobs";

  kafka.make_request("jobs", msg, function(err, results) {
    if (err) {
      console.log("in error");
      msg.error = err.data;
      return res.status(err.status).send(err.data);
    } else {
      console.log("in success");
      msg.status = results.status;
      return res.status(results.status).send(results.data);
    }
  });
});

// route.post("/apply", async (req, res) => {
//   let msg = {};
//   msg = req.body;
//   msg.auth = req.headers.authorization;
//   msg.route = "apply_jobs";

//   kafka.make_request("jobs", msg, function(err, results) {
//     if (err) {
//       console.log("in error");
//       msg.error = err.data;
//       return res.status(err.status).send(err.data);
//     } else {
//       console.log("in success");
//       msg.status = results.status;
//       return res.status(results.status).send(results.data);
//     }
//   });
// });

// route.get("/applicants", async (req, res) => {
//   console.log("----------getting jobs");
//   Decryptedtoken = decryptToken(req.headers.authorization);
//   try {
//     var companyId;
//     await Company.findOne({
//       emailId: Decryptedtoken.email
//     })
//       .then(tokenuser => {
//         if (tokenuser) {
//           companyId = tokenuser.company_basic_detail_id;
//           email = tokenuser.emailId;
//           name = tokenuser.company_name;
//         } else {
//           res.status(403).send({
//             errors: {
//               err: "Unauthenticated User"
//             }
//           });
//         }
//       })
//       .catch(err => {
//         console.log(`error getting student applicant ${err}`);
//       });

//     const result = await Job.find({
//       company_basic_detail_id: companyId
//     });
//     console.log("sending jobs-----------------" + result);
//     res.status(201).send({
//       result: result
//     });
//   } catch (err) {
//     console.log(`error getting jobs ${err}`);
//     res.status(500).send({
//       errors: {
//         body: err
//       }
//     });
//   }
// });

route.get("/applied/:statusFilter", async (req, res) => {
  let msg = {};
  msg = req.params;
  msg.auth = req.headers.authorization;
  msg.route = "applied_jobs";
  msg.query=req.query

  kafka.make_request("jobs", msg, function(err, results) {
    if (err) {
      console.log("in error");
      msg.error = err.data;
      return res.status(err.status).send(err.data);
    } else {
      console.log("in success");
      msg.status = results.status;
      return res.status(results.status).send(results.data);
    }
  });
});

route.get("/:id/students", async (req, res) => {
  let msg = {};
  msg=req.params
  msg.auth = req.headers.authorization;
  msg.route = "get_Student_By_jobs";
 

  kafka.make_request("jobs", msg, function(err, results) {
    if (err) {
      console.log("in error");
      msg.error = err.data;
      return res.status(err.status).send(err.data);
    } else {
      console.log("in success");
      msg.status = results.status;
      return res.status(results.status).send(results.data);
    }
  });
});

route.post("/:jobId/:studentId", async (req, res) => {
  let msg = {};
  msg=req.body,
  msg.params=req.params,
  msg.auth = req.headers.authorization;
  msg.route = "change_status";
 

  kafka.make_request("jobs", msg, function(err, results) {
    if (err) {
      console.log("in error");
      msg.error = err.data;
      return res.status(err.status).send(err.data);
    } else {
      console.log("in success");
      msg.status = results.status;
      return res.status(results.status).send(results.data);
    }
  });
});

module.exports = route;

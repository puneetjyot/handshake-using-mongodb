var express = require("express");
var route = express.Router();
const { generateToken, decryptToken } = require("../service/tokenservice");
const kafka = require("../kafka/client");
const { Student } = require("../db/studentmodel");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

route.get("/", async (req, res) => {
  Decryptedtoken = decryptToken(req.headers.authorization);
  try {
  if (Decryptedtoken.email !== null) {
    console.log(Decryptedtoken.email)
    await Student
      .findOne({
      
          emailId: Decryptedtoken.email
       
      })
      .then(tokenuser => {
        experience=tokenuser.experiences;
        res.status(201).send({experience})

      })
      .catch(err => {
        console.log(`error getting student basic details ${err}`);
      });
  } else {
    return res.json({
      errors: {
        message: [Decryptedtoken.error]
      }
    });
  }
  
   
   
  } catch (err) {
    res.status(403).send({
      errors: {
        err: "Unable to get experience"
      }
    });
  }
});

route.post("/", async (req, res) => {
  console.log("In posting details");
  let msg=req.body
  msg.auth=req.headers.authorization
    msg.route="post_experience"
    
    kafka.make_request("student_experience", msg, function (err, results) {
      if (err) {
        msg.error = err.data;
        
        return res.status(err.status).send(err.data);
      }
      else{
        msg.status = results.status;
         
        return res.status(results.status).send(results.data);
      }
    })
});



route.put("/", async (req, res) => {
  console.log("In posting details");
  let msg=req.body
  msg.auth=req.headers.authorization
    msg.route="edit_experience"
    
    kafka.make_request("student_experience", msg, function (err, results) {
      if (err) {
        msg.error = err.data;
        
        return res.status(err.status).send(err.data);
      }
      else{
        msg.status = results.status;
         
        return res.status(results.status).send(results.data);
      }
    })
  
});

route.delete('/', async(req,res)=>{
    console.log("deleting experience in backend");
    let msg=req.body
    msg.auth=req.headers.authorization
      msg.route="delete_experience"
      
      kafka.make_request("student_experience", msg, function (err, results) {
        if (err) {
          msg.error = err.data;
          
          return res.status(err.status).send(err.data);
        }
        else{
          msg.status = results.status;
           
          return res.status(results.status).send(results.data);
        }
      })
   
  })
module.exports = route;
